import React, { Component } from 'react';
import { connect } from 'react-redux';

import './App.css';

import { addReminder, deleteReminder, editReminders } from '../actions/index';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      dueDate: '',
      isComplete: ''
    }

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleComplateChange = this.handleComplateChange.bind(this);
    this.addReminder = this.addReminder.bind(this);
  }

  //Input Change
  handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  handleDescriptionChange(e) {
    this.setState({ description: e.target.value });
  }

  handleDateChange(e) {
    this.setState({ dueDate: e.target.value });
  }
  
  handleComplateChange(e) {
    console.log(e.target.value);
    this.setState({ isComplete: e.target.value });
  }

  addReminder() {
    const { title, description, dueDate } = this.state;
    this.props.addReminder(title, description, dueDate);

    this.setState(() => {
      return {
        title: '',
        description: '',
        dueDate: ''
      };
    });
  }

  deleteReminder(id) {
    this.props.deleteReminder(id);
  }

  editReminders(item) {
    item.isComplete = this.state.isComplete;
    this.props.editReminders(item)
  }

  renderReminders() {

    const { reminders } = this.props.reminders;

    return (
      <React.Fragment>
      <div className="sub-title">{reminders.filter(x => x.isComplete).length} COMPLATED</div>
      <ul className="list-group col-sm-6">
        {
          reminders.map((reminder) => {
            return (
              <div key={reminder.id} className="list-group-item">
                <div className="list-item">
                  <input type="checkbox" checked={this.state.isComplete} onChange={this.handleComplateChange} />
                </div>
                <div className="list-item">
                  <div>{reminder.title}</div>
                  {/* <div><em>{reminder.dueDate}</em></div> */}
                </div>

                <div className="list-item delete-button" onClick={() => this.deleteReminder(reminder.id)}>
                  &#x2715;
								</div>
              </div>
            );
          })
        }
      </ul>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className="App">
        <div className="title">
          Reminders
				</div>

        <div className="form-inline reminder-form">
          <div className="form-group">
            <input
              className="form-control"
              placeholder="Title"
              value={this.state.title}
              onChange={this.handleTitleChange}
            />
            <input
              className="form-control"
              placeholder="Description"
              value={this.state.description}
              onChange={this.handleDescriptionChange}
            />
            <input
              className="form-control"
              type="date"
              value={this.state.dueDate}
              onChange={this.handleDateChange}
            />
            <button
              type="button"
              className="btn btn-success"
              onClick={this.addReminder}
            >Add</button>
          </div>
        </div>

        {this.renderReminders()}
      </div>
    );
  }
}


const mapStateToProps = (state) => ({
  reminders: state
});


const AppWithConnect = connect(mapStateToProps, { addReminder, deleteReminder, editReminders })(App)
export default AppWithConnect


{/* // const html = (
//   <div className="App">
//     <header className="App-header row">
//       <h2 className="App-title">Reminders</h2>
//     </header>

//     <Todo />
//   </div>
// ) */}
