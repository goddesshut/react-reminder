import { combineReducers } from 'redux';
import reminders from './reminder';

export default combineReducers({
    reminders
});