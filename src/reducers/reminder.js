import { ADD_REMINDER, EDIT_REMINDERS, DELETE_REMINDER } from '../constants';
import { bake_cookie, read_cookie } from 'sfcookies';

//model
const reminder = (action) => {
	const { title, description, dueDate } = action.payload;

	return {
        title,
        description,
		dueDate,
		isComplete: false,
		id: Math.random()
	}
}

const update = (data, action) => {

	return data.map( (item, index) => {
        if(item.id !== action.payload.id) {
            return item;
        }
        // Otherwise, this is the one we want - return an updated value
        return {
            ...item,
            ...action.payload
        };    
    });
}

const reminders = (state = [], action) => {
	let reminders = null;
	state = read_cookie('reminders');
	
	switch(action.type) {
		case ADD_REMINDER:
			reminders = [...state, reminder(action)];
			bake_cookie('reminders', reminders);
			return reminders;
		case DELETE_REMINDER:
			reminders = state.filter((reminder) => reminder.id !== action.id);
			bake_cookie('reminders', reminders);
			return reminders;
		case EDIT_REMINDERS:
			reminders = update(state, action);

			console.log(reminders);
			// bake_cookie('reminders', reminders);
			return reminders;
		default:
			return state;
	}
}

export default reminders