import { ADD_REMINDER, DELETE_REMINDER, EDIT_REMINDERS } from '../constants';

export const addReminder = (title, description, dueDate) => {
	const action = {
		type: ADD_REMINDER,
		payload: {
            title,
            description,
			dueDate
		}
    };
    
	return action;
}

export const deleteReminder = (id) => {
	const action = {
		type: DELETE_REMINDER,
		id
    };
    
	return action;
}

export const editReminders = (item) => {
	const action = {
		type: EDIT_REMINDERS,
		payload: {
            title: item.title,
            description: item.description,
			dueDate: item.dueDate,
			isComplate: item.isComplate,
			id: item.id
		}
    };
    
	return action;
}
